package jwt

import (
	"crypto/hmac"
	"crypto/sha256"
	"crypto/sha512"
	"encoding/base64"
	"encoding/json"
	"errors"
	"fmt"
	"hash"
	"strings"
)

// SigningAlgorithm is a type used for to enumerate supported
// algorithms as constants
type SigningAlgorithm string

// M is just a shorthand for map[string]interface{}
type M map[string]interface{}

const (
	// HS512 is the HMAC SHA512 JWT Signing Algorithm
	HS512 SigningAlgorithm = "HS512"

	// HS256 is the HMAC SHA256 JWT Signing Algorithm
	HS256 SigningAlgorithm = "HS256"

	// Issuer is a public claim from the JWT
	// RFC that should be set to the url of
	// the issuing website/host of a token
	Issuer = "iss"

	// Subject is a public claim from the JWT
	// RFC that should be set the user/actor
	// identifier (usually a UUID or email address)
	Subject = "sub"

	// IssuedAt is a public claim from the JWT
	// RFC that should be set to the time
	// (ideally UTC) the token was created
	IssuedAt = "iat"

	// ExpiresAt is a public claim from the JWT
	// RFC that should be set to the desired
	// expiry time of a token. I recommend
	// short lived tokens with a refresh mechanism
	// to ensure control over tokens in the wild
	// and make it easy to revoke access
	ExpiresAt = "exp"

	// TokenID (jti) is a public claim from the JWT
	// RFC that should be set to the ID of this
	// newly generated token, commonly used for
	// a token refresh mechanism or white/blacklisting
	TokenID = "jti"

	// Audience is a public claim from the JWT
	// RFC that should be set to the target
	// token validator/host url
	Audience = "aud"

	// Issuer is a public claim from the JWT
	// RFC that should be set to the time the
	// token was created, or a later start timeframe
	NotBefore = "nbf"
)

// JWTToken is an object to store token information
// after parsing for validation or claim-access purposes
type JWTToken struct {
	SigningAlgorithm SigningAlgorithm
	Signature        []byte
	rawTokenBody     []byte

	Header M
	Claims M
}

// Parse parses a token string and returns a pointer
// to a JWTToken and error. Please note that this function
// merely parses the token, validation must be done after
// a JWTToken is returned.
func Parse(tokenStr string) (*JWTToken, error) {
	rawSegs := strings.Split(tokenStr, ".")
	token := JWTToken{}

	header, err := base64.RawURLEncoding.DecodeString(rawSegs[0])
	if err != nil {
		return nil, err
	}

	err = json.Unmarshal(header, &token.Header)
	if err != nil {
		return nil, err
	}

	if token.Header["typ"] != "JWT" {
		return nil, errors.New("failed to parse, not a jwt token")
	}

	switch token.Header["alg"] {
	case string(HS256):
		token.SigningAlgorithm = HS256
		break
	case string(HS512):
		token.SigningAlgorithm = HS512
		break
	default:
		return nil, errors.New("unrecognized signing algorithm")
	}

	claims, err := base64.RawURLEncoding.DecodeString(rawSegs[1])
	if err != nil {
		return nil, err
	}

	err = json.Unmarshal(claims, &token.Claims)
	if err != nil {
		return nil, err
	}

	sig, err := base64.RawURLEncoding.DecodeString(rawSegs[2])
	if err != nil {
		return nil, err
	}

	token.Signature = sig
	token.rawTokenBody = []byte(fmt.Sprintf("%s.%s", rawSegs[0], rawSegs[1]))

	return &token, nil
}

// VerifySignature compares the signature of a token
// to a newly generated signature using the supplied
// secret.
func (j *JWTToken) VerifySignature(secret []byte) bool {
	var mac hash.Hash

	switch j.SigningAlgorithm {
	case HS256:
		mac = hmac.New(sha256.New, secret)
		break
	case HS512:
		mac = hmac.New(sha512.New, secret)
		break
	default:
		return false
	}

	mac.Write(j.rawTokenBody)
	expectedMAC := mac.Sum(nil)
	return hmac.Equal(j.Signature, expectedMAC)
}

// New generates and signs a new JWT token with the supplied algorithm.
// secret and claims object, returning a string and error.
func New(alg SigningAlgorithm, secret []byte, claims M) (string, error) {
	header := M{
		"typ": "JWT",
		"alg": alg,
	}

	headerEncoded, err := json.Marshal(header)
	if err != nil {
		return "", err
	}

	b64Header := base64.RawURLEncoding.EncodeToString(headerEncoded)

	claimsEncoded, err := json.Marshal(claims)
	if err != nil {
		return "", err
	}

	b64Claims := base64.RawURLEncoding.EncodeToString(claimsEncoded)
	jwtStr := fmt.Sprintf("%s.%s", b64Header, b64Claims)

	var mac hash.Hash
	switch alg {
	case HS256:
		mac = hmac.New(sha256.New, secret)
		break
	case HS512:
		mac = hmac.New(sha512.New, secret)
		break
	}

	mac.Write([]byte(jwtStr))
	macSig := mac.Sum(nil)
	b64Sig := base64.RawURLEncoding.EncodeToString(macSig)

	return fmt.Sprintf("%s.%s", jwtStr, string(b64Sig)), nil
}
